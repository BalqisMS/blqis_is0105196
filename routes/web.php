<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/event', function () {
   return view('event');

  
   Route::get('/eventlist', [App\Http\Controllers\EventController::class, 'index'])->name('eventlist');
   Route::post('/eventlist',[App\Http\Controllers\EventController::class, 'save']);
  
});



/*Route::post('event', function () {
    return view('event');
});
*/



Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
Route::get('admin/home', [App\Http\Controllers\adminHomeController::class, 'adminHome'])->name('admin.home')->middleware('is_admin');
/*
Route::get('/eventlist', [App\Http\Controllers\EventController::class, 'index'])->name('eventlist');;
Route::get('/event',[App\Http\Controllers\EventController::class, 'create'])->name('event');  
Route::post('/eventlist',[App\Http\Controllers\EventController::class, 'save'])->name('eventlist');  
Route::get('/eventlist',[App\Http\Controllers\EventController::class, 'show'])->name('event');  
Route::get('/event',[App\Http\Controllers\EventController::class, 'create'])->name('event');  
Route::get('/event',[App\Http\Controllers\EventController::class, 'create'])->name('event');  
Route::get('/eventedit',[App\Http\Controllers\EventController::class, 'edit'])->name('eventedit');  
Route::patch('/eventlist',[App\Http\Controllers\EventController::class, 'update'])->name('eventlist');  
Route::delete('/eventlist',[App\Http\Controllers\EventController::class, 'destroy'])->name('event');  
*/


