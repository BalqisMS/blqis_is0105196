<?php

namespace App\Http\Controller;
use Illuminate\Http\Request;
use App\Models\Event;

class EventController extends Controller
{
   
    public function index()
    {
       $events = Event::all()->toArray();
        return view('eventview',compact('event'));
    }


    public function event()
    {
       $events = Event::all();
        return view('event');
    }


    public function create(array $data)
    {
       return view('event');
    }

    public function save(Request $request)
    {
        //print_r($request->input());
        $event = new Event;
        $event->ename= $request->ename;
        $event->description= $request->description;
        echo $event->save();

        return view('eventlist');
        /*
       $this->validate($request, [
       'ename' => 'required',
       'description' => 'required'
       ]); 
       $event = new Event([
           'ename' => $request ->get('ename'),
           'description' => $request ->get('description'),
       ]);
       $event->save();
       return redirect()->route('/eventlist') ->with('success', 'New event added');
       */
    }
 
    public function show($id)
    {
        return view('eventlist'); 
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        {
            $event = Event::find($id);
            return view('eventlist');        
        }
    
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'ename'=>'required',
            'description'=>'required',
            
        ]);

        $event = Event::find($id);
        $event->ename =  $request->get('ename');
        $event->description = $request->get('description');
        $event->save();

        return redirect('/eventlist')->with('success', 'Event updated!');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $event = Event::find($id);
        $event->delete();

        return redirect('/eventlist')->with('success', 'Event deleted!');

    }
}
