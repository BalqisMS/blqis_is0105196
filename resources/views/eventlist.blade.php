($events =$events::all();
return view($events.index', compact($events'));


@extends('layouts.admin')

@section('content')
<div class="main"> 
<div class="alert alert-success alert-dismissible">
             <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                New event added.
            </div>
</div>          

<div class="row">
<div class="col-sm-12">
    <h1 class="display-3">Events</h1>    
  <table class="table table-striped">
    <thead>
        <tr>
          <td>ID</td>
          <td>Name</td>
          <td>Description</td>

          <td colspan = 2>Actions</td>
        </tr>
    </thead>
    <tbody>
        @foreach($events as $event)
        <tr>
            <td>{{$event->id}}</td>
            <td>{{$event->ename}}</td>
            <td>{{$event->description}}</td>
            <td>
                <a href="{{ route($events.edit',$events->id)}}" class="btn btn-primary">Edit</a>
            </td>
            <td>
                <form action="{{ route($events.destroy', $events->id)}}" method="post">
                  @csrf
                  @method('DELETE')
                  <button class="btn btn-danger" type="submit">Delete</button>
                </form>
            </td>
        </tr>
        @endforeach
    </tbody>
  </table>
<div>
</div>
@endsection


@endsection