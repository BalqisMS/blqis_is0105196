@extends('layouts.admin')

@section('content')

<div class="main">           
            <div class="alert alert-success alert-dismissible">
             <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                <strong>Welcome!</strong> You are logged in as Admin.
            </div>
           
            <div class="card">
                <div class="card-header">Dashboard</div>
                <div class="card-body">
                @if (session('is_admin'))
                        <div class="alert alert-success" role="alert">
                            {{ session('is_admin') }}
                        </div>
                    @endif
                    <table class="table">
                        <thead>
                            <tr>
                                <td>Name</td>
                                <td>Email</td>
                                <td>Status</td>
                                <td>Action</td>
                            </tr>
                        </thead>
                
                    </table>
                </div>
            </div>
           

               
    

</div>

@endsection

