

@extends('layouts.admin')
@section('content')


<div class="container1">
<div class="main">
    <br/>
<h1>Add New Event</h1>
<br/>

<form  action="{{ url('/eventlist') }}" method="POST">
   @csrf
   
  <div class="form-group">
    <label for="ename">Event name:</label>
    <input type="text" class="form-control" name="ename"  placeholder="Enter event name">
    </div> 



<div class="form-group">
  <label for="description">Description:</label>
  <textarea class="form-control" rows="5" name="description"></textarea>
</div>
  
  <button type="submit" class="btn btn-primary">Submit</button>
</form>
</div>
</div>
</html>
@endsection